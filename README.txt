-- SUMMARY --

The Image Pinterest module allows administrators to set images to include a pinterest
button per-field by adding an option to the core image formatter.

For a full description of the module, visit the project page:
  http://drupal.org/project/image_pinterest

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/image_pinterest


-- REQUIREMENTS --

Module Field Formatter Settings (https://drupal.org/project/field_formatter_settings)
must be installed.


-- CONFIGURATION --

* Configure formatter settings in Structure » Content Types » Manage Display:

  - Use the Image formatter

    Select your image style preset and link preferences as usual.  Below them
    is a checkbox to enable Pinterest for this field instance.


-- CONTACT --

Current maintainers:
* Derek Adams (delzhand) - https://drupal.org/user/177088

This project has been sponsored by:
* Kilpatrick Design
  http://kilpatrickdesign.com
