(function ($) {

Drupal.behaviors.pinterest_image = {
  attach: function(context, settings) {
    $('[data-pin="true"]').once('pinterestImage').append($('<div class="pinterest-button"><a href="//www.pinterest.com/pin/create/button/?url=' + $(this).find('img').attr('src') + '" data-pin-do="buttonPin" data-pin-config="none" data-pin-color="white" data-pin-height="28"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_28.png"/></a></div>'
    )).css('position', 'relative');
  }
}

})(jQuery);
